package com.example.practica02kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnCerrar: Button? = null
    private var lblImc: TextView? = null
    private var txtPeso: EditText? = null
    private var txtAltura: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCalcular = findViewById<View>(R.id.btnCalcular) as Button
        btnLimpiar = findViewById<View>(R.id.btnLimpiar) as Button
        btnCerrar = findViewById<View>(R.id.btnCerrar) as Button
        lblImc = findViewById<View>(R.id.lblImc) as TextView
        txtPeso = findViewById<View>(R.id.txtPeso) as EditText
        txtAltura = findViewById<View>(R.id.txtAltura) as EditText

        btnCalcular!!.setOnClickListener{
            val pesoString = txtPeso!!.text.toString()
            val alturaString = txtAltura!!.text.toString()

            if (pesoString.isNotEmpty() && alturaString.isNotEmpty()) {
                val peso = pesoString.toDouble()
                val altura = alturaString.toDouble()

                val imc = peso / (altura * altura)
                val imcFormatted = String.format("%.1f", imc)

                lblImc!!.text = "$imcFormatted kg/m²"
            } else {
                lblImc!!.text = "Ingrese el peso y la altura"
            }
        }

        btnLimpiar!!.setOnClickListener{
            lblImc!!.text = ""
            txtPeso!!.setText("")
            txtPeso!!.requestFocus()
            txtAltura!!.setText("")
        }
        btnCerrar!!.setOnClickListener{
            finish()
        }
    }
}