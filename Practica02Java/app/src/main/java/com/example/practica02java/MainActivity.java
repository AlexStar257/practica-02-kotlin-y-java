package com.example.practica02java;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;
    private TextView lblImc;
    private EditText txtPeso;
    private EditText txtAltura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        lblImc = findViewById(R.id.lblImc);
        txtPeso = findViewById(R.id.txtPeso);
        txtAltura = findViewById(R.id.txtAltura);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pesoString = txtPeso.getText().toString();
                String alturaString = txtAltura.getText().toString();

                if (!pesoString.isEmpty() && !alturaString.isEmpty()) {
                    double peso = Double.parseDouble(pesoString);
                    double altura = Double.parseDouble(alturaString);

                    double imc = peso / (altura * altura);
                    String imcFormatted = String.format("%.1f", imc);

                    lblImc.setText(imcFormatted + " kg/m²");
                } else {
                    lblImc.setText("Ingrese el peso y la altura");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblImc.setText("");
                txtPeso.setText("");
                txtPeso.requestFocus();
                txtAltura.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
